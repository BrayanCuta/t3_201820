package controller;

import api.IDivvyTripsManager;
import model.data_structures.DoublyLinkedList;
import model.data_structures.IDoublyLinkedList;
import model.logic.DivvyTripsManager;
import model.vo.VOTrip;

public class Controller {

	/**
	 * Reference to the services manager
	 */
	private static DivvyTripsManager  manager = new DivvyTripsManager();
	
	public static void loadStations() {
		manager.loadStations("./Data/Divvy_Stations_2017_Q3Q4.csv");
	}
	
	public static void loadTrips() {
		manager.loadTrips("./Data/Divvy_Trips_2017_Q4.csv");
	}
		
	public static DoublyLinkedList <String> getLastNStations (int bicycleId, int n) {
		return manager.getLastNStations(bicycleId, n);
	}
	
	public static VOTrip customerNumberN (int stationID, int n) {
		
		return manager.customerNumberN(stationID, n);
	}
}
