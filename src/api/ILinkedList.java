package api;

public interface ILinkedList<T> extends Iterable<T> {
	
	
	/**
	 * A�ade el elemento al final de la lista
	 */
	void addEnd (T item);
	
	/**
	 * A�ade el elemento al princio de la lista
	 * @param item
	 */
	void addFirst(T item);
	
	
	/**
	 * Elimina un elemento en la posicion dado
	 */
	void delete(int k);
	
	/**
	 * Consulta el tama�o de una lista
	 * @return
	 */
	Integer getSize();
	
	/**
	 * Recupera un elemento de la lista dada una posicion
	 */
	T getElement (int posicion);
	
	/**
	 * Consulta elemento acutal y lo retorna
	 */
	T getCurrentElement ();
	
	/**
	 * Actualiza el nodo actual al pr�ximo nodo
	 * @return
	 */
	T next ();
	

}