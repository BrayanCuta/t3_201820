package api;

/**
 * Abstract Data Type for a doubly-linked list of generic objects
 * This ADT should contain the basic operations to manage a list
 * add, addAtEnd, AddAtK, getElement, getCurrentElement, getSize, delete, deleteAtK
 * next, previous
 * @param <T>
 */
public interface IDoublyLinkedList<T> extends Iterable<T> {
	
	/**
	 * Retrocede un nodo en la lista
	 * @return
	 */
	void previous ();
	
	/**
	 * Avanza un nodo en la lista
	 * @return
	 */
	void next ();

	/**
	 * Elimina el elemento dada la posicion 
	 */
	void deleteAtK (int k);
	
	/**
	 * Elimina el elemento actual
	 */
	void delete();
	
	/**
	 * Consulta elemento acutal y lo retorna
	 */
	T getCurrentElement ();
	
	/**
	 * Recupera un elemento de la lista dada una posicion
	 */
	T getElement (int posicion);
	
	/**
	 * A�ada un elemento al final  de la lista
	 * @param item
	 */
	void addAtEnd (T item);
	
	/**
	 * A�ade un elemento a la lista
	 * @param item
	 */
	void add(T item);

	/**
	 * Consulta si la lista esta vacia
	 * @return
	 */
	boolean isEmpty ();
	
	/**
	 * Consulta el tama�o de una lista
	 * @return
	 */
	Integer getSize();

}
