package model.vo;

import java.util.Date;

public class VOStation {
	
	private int id;
	private String name;
	private String city;
	private double latitude;
	private double longitude;
	private int dpCacity;
	private Date onlineDate;
	
	
	public void setName (String pName){
		name = pName;
	}
	public String getName (){
		return name;
	}
	
	public void setOnlineDate (Date pDate){
		onlineDate = pDate;
	}
	public Date getOnlineDate (){
		return onlineDate;
	}
	
	public void setDpCapacity (int pCapacity){
		dpCacity = pCapacity;
	}
	public int getDpCapacity (){
		return dpCacity;
	}
	
	public void setLongitude (double pLongitude){
		longitude = pLongitude;
	}
	public double getLongitude (){
		return longitude;
	}
	
	public void setLatitude (double pLatitude){
		latitude = pLatitude;
	}
	public double getLatitude (){
		return latitude;
	}
	
	public void setCity (String pCity){
		city = pCity;
	}
	public String getCity (){
		return city;
	}
	
	public void setId (int pId){
		id = pId;
	}
	public int getId (){
		return id;
	}
	

}
