package model.vo;

import java.util.Date;

/**
 * Representation of a Trip object
 */
public class VOTrip {

	private int id ;
	private Date start_time;
	private Date stop_time;
	private int bikeId;
	private double tripSeconds;
	private int fromStationId;
	private String fromStation;
	private int toStationId;
	private String toStation;
	private String userType;
	private String gender;
	private int birthYear;
	
	
	public int getToStationId (){
		return toStationId;
	}
	public void setToStationId (int setId){
		toStationId = setId;
	}
	
	public int getFromStationId (){
		return fromStationId;
	}
	public void setFromStationId (int setId){
		fromStationId = setId;
	}
	
	public void setBirthYear (int pBirthYear){
		birthYear = pBirthYear;
	}
	public int getBirthYear (){
		return birthYear;
	}
	
	
	public void setGender (String pGender){
		gender = pGender;
	}
	public String getGender (){
		return gender;
	}
	
	public void setUsertype (String pUserType){
		userType = pUserType;
	}
	public String getUserType (){
		return userType;
	}
	
	
	public void setBikeId (int pBikeId){
		bikeId = pBikeId;
	}
	public int getBikeId (){
		return bikeId;
	}
	
	
	public void setStop_time (Date pStopTime){
		stop_time = pStopTime;
	}
	public Date getStop_time (){
		return stop_time;
	}
	
	public void setStart_time (Date pStart_time){
		
		start_time = pStart_time;
		
	}
	public Date getStart_time (){
		return start_time;
	}
	
	
	
	public void setId (int pId){
		id = pId;
	}
	/**
	 * @return id - Trip_id
	 */
	public int id() {
		return id;
	}	
	
	
	public void setTripSeconds (double pSeconds){
		tripSeconds = pSeconds;
	}
	/**
	 * @return time - Time of the trip in seconds.
	 */
	public double getTripSeconds() {
		return tripSeconds;
	}

	
	public void setFromStation (String pStation){
		fromStation = pStation;
	}
	/**
	 * @return station_name - Origin Station Name .
	 */
	public String getFromStation() {
		return fromStation;
	}
	
	public void setToStation (String pStation){
		toStation = pStation;
	}
	/**
	 * @return station_name - Destination Station Name .
	 */
	public String getToStation() {
		return toStation;
	}
}
