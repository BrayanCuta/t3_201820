package model.vo;

/**
 * Representation of a byke object
 */
public class VOBike {

	private int id;
	
	/**
	 * @return id_bike - Bike_id
	 */
	public int id() {
		return id;
	}	
}
