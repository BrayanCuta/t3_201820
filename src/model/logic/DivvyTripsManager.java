package model.logic;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.Stack;

import api.IDivvyTripsManager;
import model.vo.VOStation;
import model.vo.VOTrip;
import model.data_structures.DoublyLinkedList;
import model.data_structures.IDoublyLinkedList;
import model.data_structures.List;
import model.data_structures.Queue;

public class DivvyTripsManager implements IDivvyTripsManager {
	
	private List<VOTrip> listTrips;
	private List<VOStation> listStations;
	private Queue<VOTrip> queueTrips;
	private Stack<VOTrip> stackTrips;
	
	
	public Stack<VOTrip> getStackTrips (){
		return stackTrips;
	}

	public void loadStations (String stationsFile) {
		listStations = new List<>();

		String separador = ",";
		BufferedReader br = null;

		try { 

			br = new BufferedReader(new FileReader(stationsFile));
			String line = br.readLine();

			line = br.readLine();

			while (line != null){

				VOStation voActual =  new VOStation();
				String [] fields = line.split(separador);

				voActual.setId(Integer.parseInt(fields[0]));
				voActual.setName(fields[1]);
				voActual.setCity(fields[2]);
				voActual.setLatitude(Double.parseDouble(fields[3]));
				voActual.setLongitude(Double.parseDouble(fields[4]));
				voActual.setDpCapacity(Integer.parseInt(fields[5]));

				String date = fields[6];
				Date newFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm").parse(date);
				voActual.setOnlineDate(newFormat);

				listStations.agregarElementoFinal(voActual);

				line = br.readLine();


			}

		}catch (Exception e) {
			// TODO: handle exception
		} 
		if (br == null) {try {
			br.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}}

	}

	public void loadTrips (String tripsFile) {
		listTrips = new List<>();
		queueTrips = new Queue<>();
		stackTrips = new  Stack<>();
		
		String separador = ",";
		BufferedReader br = null;
		
		try {
			
			 br = new BufferedReader(new FileReader(tripsFile));
			 String line = br.readLine();
			
			 line = br.readLine();
			 
			 while (null != line){
				 
				 
				 VOTrip voActual =  new VOTrip();
				 String [] fields = line.split(separador);
				 
				 voActual.setId(Integer.parseInt(fields[0]));
				 
				 String date1 = fields [1];
				 Date newDate1 = new SimpleDateFormat("dd/MM/yyyy hh:mm").parse(date1);
				 voActual.setStart_time(newDate1);
				 
				 String date2 = fields [2];
				 Date newDate2 = new SimpleDateFormat("dd/MM/yyyy hh:mm").parse(date2);
				 voActual.setStart_time(newDate2);
				 
				 voActual.setBikeId(Integer.parseInt(fields[3]));
				 voActual.setTripSeconds(Double.parseDouble(fields[4]));
				 voActual.setFromStationId(Integer.parseInt(fields[5]));
				 voActual.setFromStation(fields[6]);
				 voActual.setToStationId(Integer.parseInt(fields[7])); 
				 voActual.setToStation(fields[8]);
				 voActual.setUsertype(fields[9]); 
				 
				 if (fields.length == 11)
				 voActual.setGender(fields[10]);
				 
				 else if (fields.length == 12)
				 {voActual.setGender(fields[10]);
				 voActual.setBirthYear(Integer.parseInt(fields[11]));}
				
				 
				 
				 listTrips.agregarElementoFinal(voActual);
				 queueTrips.enqueue(voActual);
				 stackTrips.push(voActual);
				 
				 line = br.readLine();
			 }
			 
		}
		catch (Exception e) {
			// TODO: handle exception
		} 
		if (br == null) {try {
				br.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}}

	}

	@Override
	public DoublyLinkedList <String> getLastNStations (int bicycleId, int n) {
		DoublyLinkedList<String> rta = new DoublyLinkedList<>();
		Iterator<VOTrip> iter = queueTrips.iterator();
		while (iter.hasNext()){
			VOTrip act = iter.next();
			if (bicycleId ==  act.getBikeId() && rta.getSize() < n){
				String nameStation = act.getFromStation();
				rta.add(nameStation);
			}
		}
		return rta;
	}

	@Override
	public VOTrip customerNumberN (int stationID, int n) {
		VOTrip rta = new VOTrip();
		Iterator<VOTrip> iter = stackTrips.iterator();
		int contador = 0;
		while (iter.hasNext() && contador <n){
			VOTrip act = iter.next();
//			System.out.println(act.getFromStationId());
			if (act.getToStationId() == stationID){
				
				contador = contador +1;
				if (contador == n){
					 rta = act;
				}


			}
		}
		return rta;

	}	
	
//	public static void main(String[] args) {
//		DivvyTripsManager prueba = new DivvyTripsManager();
//		prueba.loadTrips("./Data/Divvy_Trips_2017_Q4.csv");
//		
//		VOTrip rta =prueba.customerNumberN(341, 1);
//		
//		System.out.println(rta.g);
//		
//		Iterator<VOTrip> iter = prueba.getStackTrips().iterator();
//		while (iter.hasNext()){
//			VOTrip act = iter.next();
//			System.out.println(act.getToStationId());
//		}
//		
//	}


}
