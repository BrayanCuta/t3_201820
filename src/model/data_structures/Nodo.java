package model.data_structures;

public class Nodo<T> 
{
	private T item;
	private Nodo<T> next;
	
	public Nodo(T item)
	{
		this.item = item;
		this.next = null;
		
	}
	
	public Nodo(T item, Nodo<T> post)
	{
		this.item = item;
		this.next = post;
		
	}
	
	public T getItem() { return item; }

	public Nodo<T> getNext() { return next; }

	
	public void setItem(T item) { this.item = item;}
	
	public void setNext(Nodo<T> next) { this.next = next; }

	
	public T get(int pos)
	{
		if(pos == 0)
			return this.item;
		else
			return item;
	}


}

