package model.data_structures;

public class DoublyNode <E> {
	
	
	private E item;
	private DoublyNode<E> next;
	private DoublyNode<E> prev;
	
	
	public DoublyNode (E item) {
		this.item = item;
		next = null;
		prev = null;}
	
	public DoublyNode<E> getNext() {
		return next;}
	
	public DoublyNode<E> getPrev (){
		return prev;
	}
	
	public void setNextNode ( DoublyNode<E> next) {
		this.next = next;}
	
	public void setPreNode (DoublyNode<E> pre){
		this.prev = pre;
	}
	
	public E getItem(){
		return item;}
	
	public void setItem (E item) {
		this.item = item;}

}
