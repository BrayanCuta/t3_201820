package model.data_structures;
import java.util.Iterator;
import api.ILinkedList;
import model.data_structures.Node;


public class LinkedList<T> implements ILinkedList<T>   {
	
	private Node<T> list,cursorIter;
	private int listSize;
	
	public  LinkedList() {
		list = null;
	}

	@Override
	public Iterator<T> iterator() {
		cursorIter = list;
			
			return new Iterator<T>() {
				
				@Override
				public boolean hasNext() {
					return cursorIter!=null;
				}
				@Override
				public T next(){
					Node<T> a=cursorIter;
					cursorIter=a.getNext();
					return a.getItem();
					}
			};
			
		
	}

	@Override
	public void addFirst(T item) {
		Node<T> newHead = new Node<T>(item);
		newHead.setNextNode(list);
		list = newHead;
		listSize ++;
		
	}
	
	@Override
	public void addEnd(T item) {
		Node <T> newNode = new Node<> (item);
		if(list == null) {
			list = newNode;
		}
		else {
			Node <T> actual = list;
			while(actual.getNext() != null) {
				actual = actual.getNext();
			}
			actual.setNextNode(newNode);
		}
		listSize++;

		
	}

	@Override
	public void delete(int posicion) {
		Node<T> iter = new Node<T>(null);
		iter = list;
		
		
		if (posicion == 1){
			list = list.getNext();
		}
		
		
		 if( posicion == listSize || listSize >0){
			
			for (int i =1 ; i <posicion-1; i++)
				{
				
				iter = iter.getNext();
	
				}
	
				Node<T> next = iter.getNext().getNext();

				iter.setNextNode(next);
			}
			
	}
		
	


	@Override
	public Integer getSize() {
		return listSize;
	}

	@Override
	public T getElement(int posicion) {
		Node<T> iter = new Node<T>(null);
		iter = list;
		
		if (posicion == 1){
			return iter.getItem();
		}
		else {
			
			for(int i = 0 ; i <posicion-1;i ++){
				iter = iter.getNext();
			}
			return iter.getItem();
		}
		
	}
	

	@Override
	public T getCurrentElement() {
		return list.getItem();
	}

	@Override
	public T next() {
		return list.getNext().getItem();
	}
	
	public boolean isEmpty() {
		return listSize ==0 ;
	}
 
	
	
	public static void main(String[] args) {
		LinkedList<String > prueba = new LinkedList<>();
		prueba.addEnd("a");
		prueba.addEnd("b");
		prueba.addEnd("c");
		prueba.addEnd("d");
		
		System.out.println(prueba.getElement(5));


//		Iterator<String> iter = prueba.iterator();
//		while(iter.hasNext()){
//			System.out.println(iter.next());
//		}
		
		
		
	}




	
	

}
