package model.data_structures;

import java.util.Iterator;
import api.IDoublyLinkedList;

public class DoublyLinkedList <T> implements IDoublyLinkedList<T> {
	
	private DoublyNode<T> prev, post, cursor;
	
	private int size;
	
	 public DoublyLinkedList() {
		cursor = new DoublyNode<T>(null);
		 
		prev = new DoublyNode<T>(null);
		post = new DoublyNode<T>(null);
		prev.setNextNode(post);
		post.setPreNode(prev);
		
		
	}
	 
	 
	public DoublyNode<T> getPrev (){
		return prev;
	}
	
	public DoublyNode<T> getPost (){
		return post;
	}
	
	@Override
	public Iterator<T> iterator() {
		cursor = prev.getNext();
		return new Iterator<T>() {
			
			@Override
			public boolean hasNext() {
				return cursor.getNext()!=null;
			}
			@Override
			public T next(){
				DoublyNode<T> a=cursor;
				cursor=a.getNext();
				return a.getItem();
				}
		};
		
	}

	@Override
	public void previous() {
		DoublyNode<T> act = post.getPrev();
		post = act;
	}

	@Override
	public void next() {
		DoublyNode<T> act = prev.getNext();
		prev = act;
		
	}

	@Override
	public void deleteAtK(int k) {
		DoublyNode<T> iter = new DoublyNode<T>(null);
		iter = prev.getNext();
		
		
		if (k == 1){
			prev = prev.getNext();
		}
		
		
		 if( k == size || size >0){
			
			for (int i =1 ; i <k-1; i++)
				{
				
				iter = iter.getNext();
	
				}
	
			DoublyNode<T> next = iter.getNext().getNext();

				iter.setNextNode(next);
				next.setPreNode(iter);
			}
	
		
	}

	@Override
	public void delete() {
		prev.setNextNode(prev.getNext().getNext());
		post.setPreNode(post.getPrev().getPrev());
		
	}

	@Override
	public T getCurrentElement() {
		return cursor.getItem();
	}

	@Override
	public T getElement(int posicion) {
		DoublyNode<T> iter = new DoublyNode<T>(null);
		iter = prev.getNext();
		
		if (posicion == 1){
			return iter.getItem();
		}
		else {
			
			for(int i = 0 ; i <posicion-1;i ++){
				iter = iter.getNext();
			}
			return iter.getItem();
		}
		
	}

	@Override
	public void addAtEnd(T item) {
		DoublyNode<T> last = post.getPrev();
		DoublyNode<T> x = new DoublyNode<T>(item);
		x.setNextNode(post);
		x.setPreNode(last);
		post.setPreNode(x);
		last.setNextNode(x);
		size ++;
		cursor = post.getPrev().getPrev();
	}

	@Override
	public void add(T item) {
		DoublyNode<T> next1 = prev.getNext();
		DoublyNode<T> x = new DoublyNode<T>(item);
		x.setPreNode(prev);
		x.setNextNode(next1);
		prev.setNextNode(x);
		next1.setPreNode(x);
		
		
		size ++;
		cursor = prev.getNext().getNext();
		
	}

	@Override
	public boolean isEmpty() {
		return size == 0;
	}

	@Override
	public Integer getSize() {
		return size;
	}
	
//	public static void main(String[] args) {
//		DoublyLinkedList<String> prueba = new DoublyLinkedList<>();
//		prueba.add("a");
//		prueba.add("b");
//		prueba.add("c");
//		prueba.add("d");
//		
//		System.out.println("Elemento numero 2 : "+prueba.getElement(2));
//		prueba.delete();
//		System.out.println("Prev :" + prueba.getPrev().getNext().getItem());
//		System.out.println("Post :" + prueba.getPost().getPrev().getItem());
//		prueba.next();
//		
//		System.out.println("actual debe ser c =" + prueba.getCurrentElement());
//		
//		Iterator<String> iter = prueba.iterator();
//		while (iter.hasNext()){
//			System.out.println(iter.next());
//		}
//		
//	}

}
